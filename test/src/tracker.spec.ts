import { random } from "faker";
import { expect } from "chai";
import { get } from "request-promise-native";
import { dbQuery, syncDB } from "./utils/db";

const TRACKER_HOST = process.env["TRACKER_HOST"] || "localhost";
const TRACKER_PORT = process.env["TRACKER_PORT"] || "3000";

describe('Tracker server', () => {
  // before((done) => {
  //   setTimeout(done, 10000);
  // });

  beforeEach( async() => {
    await syncDB();

    // reset DB
    await dbQuery({
      type: "remove",
      table: "track_events"
    });

    // feed data
    this.tracks = (await dbQuery({
      type: "insert",
      table: "track_events",
      values: {
        "rider_id": 4,
        "north": random.number({min: 1, max: 20}),
        "south": random.number({min: 1, max: 20}),
        "east": random.number({min: 1, max: 20}),
        "west": random.number({min: 1, max: 20}),
        "createdAt": new Date(),
        "updatedAt": new Date()
      },
      returning: [ "rider_id", "north", "south",
        "west", "east", "createdAt"
      ]
    }))[0][0];
  });

  describe('Movement', () => {
    it('Harusnya memberikan data suatu rider', async() => {
      const response = await get(
        `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`,
        { json: true }
      );
      expect(response.ok).to.be.true;
      expect(response.logs).to.be.deep.eq([{
        time: this.tracks.createdAt.toISOString(),
        north: this.tracks.north,
        south: this.tracks.south,
        east: this.tracks.east,
        west: this.tracks.west,
      }]);
    })
  })
})